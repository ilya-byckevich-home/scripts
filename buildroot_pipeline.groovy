pipeline {
            parameters {
            string(name: 'PACKAGE_PATH', defaultValue: '')
            string(name: 'CONFIG_NAME', defaultValue: '')
        }
        
    agent none
    stages {
        stage('Checkout project') {
            agent any
            steps {
                git branch: 'master',
                    url: 'https://git.busybox.net/buildroot'

                script{
                    
                    /* Just for tests */
                    echo "from Buildroot!"
                    echo "${env.PACKAGE_PATH}"
                    echo "${env.CONFIG_NAME}"
                    
                    /* Copy params to local shell */
                    PACKAGE_PATH = env.PACKAGE_PATH
                    CONFIG_NAME = env.CONFIG_NAME
                    sh 'if [ ! -d build ];then mkdir build; fi'
                    sh 'make O=build BR2_EXTERNAL=${PACKAGE_PATH} raspberrypi3_64_defconfig'
                    sh 'echo "${CONFIG_NAME}=y" >> build/.config'
                    sh 'tail build/.config -n 5'
                    sh 'make O=build -j10'
                }

            }
             post {
                success{
                    archiveArtifacts artifacts: 'build/images/sdcard.img'
                }
            }
        }
    }
}
