pipeline {
    agent none
    
    stages {
        stage('Get cross compiler') {
            agent any

            steps {
                script{
                    arch_name="gcc-arm-none-eabi-8-2019-q3-update-linux.tar.bz2"
                    dir_name = arch_name.split("-linux")[0];
                    
                    sh "if [ ! -d ${dir_name} ]; then wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/8-2019q3/RC1.1/${arch_name}; tar xjf ${arch_name}; fi"

                    env.COMPILER_PATH=env.WORKSPACE+"/${dir_name}/bin"
                }
            }
        }
    }
}
