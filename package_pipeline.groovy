pipeline {
    agent none
    stages {
        stage('Checkout external proj') {
            agent any
            steps {
                git branch: 'new',
                    credentialsId: '689f319f-9369-4bef-aa7e-09015952b22e',
                    url: 'https://ilya-byckevich-home@bitbucket.org/ilya-byckevich-home/foo_package.git'

                script{
                    env.PACKAGE_PATH=env.WORKSPACE
                    env.CONFIG_NAME="BR2_PACKAGE_FOO_PACKAGE"
                }

                script{     
                	echo "--- CppCheck ---"           
			    	sh 'cppcheck --enable=all --inconclusive --xml --xml-version=2 . --std=c11  2> ${JOB_NAME}_cppcheck.xml'
			        publishCppcheck pattern: "${JOB_NAME}"+'_cppcheck.xml'
				}
            }
        }
    }
}
