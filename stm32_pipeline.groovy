pipeline {
            parameters {
            string(name: 'COMPILER_PATH', defaultValue: '')
        }
    agent none
    stages {
        stage('Build for stm32') {
            agent any
            steps {
                git branch: 'master',
                    credentialsId: '689f319f-9369-4bef-aa7e-09015952b22e',
                    url: 'https://ilya-byckevich-home@bitbucket.org/ilya-byckevich-home/stm32_test.git'

                script{
                    echo "${env.COMPILER_PATH}"
                    COMPILER_PATH = env.COMPILER_PATH
                    sh 'export PATH=$PATH:${COMPILER_PATH}; cd arm_build; make all;'
                }

                script{     
                	echo "--- CppCheck ---"           
			    	sh 'cppcheck --enable=all --inconclusive --xml --xml-version=2 . --std=c11  2> ${JOB_NAME}_cppcheck.xml'
			        publishCppcheck pattern: "${JOB_NAME}"+'_cppcheck.xml'
				}
            }
            post {
                success{
                    archiveArtifacts artifacts: 'arm_build/stm32_test.*'
                }
            }
        }
/*
        stage('Artifacts transfer') {
            agent any
            steps {
             script {
              sshPublisher(
               continueOnError: false, failOnError: true,
               publishers: [
                sshPublisherDesc(
                 configName: "ilya-home",
                 verbose: true,
                 transfers: [
                  sshTransfer(
                   sourceFiles: "arm_build/stm32_test.*",
                   //remoteDirectory: "",
                   execCommand: 'echo "ok"'
                    )
                 ])
               ])
             }
            }
           post {
            success{
                echo "Artifacts successfully sent!"
            }
            failure{
                echo "Oh no!"
            }
          }
        }
*/
    }
}
