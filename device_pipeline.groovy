pipeline {

    parameters {
        string(name: 'PACKAGE_PATH', defaultValue: '')
        string(name: 'CONFIG_NAME', defaultValue: '')
    }
    agent none 

    stages {
        stage('parallel jobs'){ 
            parallel{
                stage('Buildroot'){      
                    agent any 
                    steps {
                        script {
                        /*
                            echo "Test end"
                            echo repository_name
                            echo commit_msg
                            echo "Test end"
                        */
                            def build_var = build job:'TestPackage'
                            vars = build_var.getBuildVariables();
                            env.PACKAGE_PATH = vars.get('PACKAGE_PATH')
                            env.CONFIG_NAME = vars.get('CONFIG_NAME')
                        }
                        script{
                            build job: 'BuildrootPipe', parameters:[
                                string(name: 'PACKAGE_PATH', value: env.PACKAGE_PATH),
                                string(name: 'CONFIG_NAME', value: env.CONFIG_NAME)
                             ]
                        }
                    }
                }

                stage('Stm32'){
                    agent any
                    steps {
                        script {
                            def build_var = build job:'CrossCompiler'
                            vars = build_var.getBuildVariables();
                            env.COMPILER_PATH = vars.get('COMPILER_PATH')
                        }
                        script{
                            echo "${env.COMPILER_PATH}"
                            build job: 'Stm32Pipe', parameters:[
                                string(name: 'COMPILER_PATH', value: env.COMPILER_PATH),
                             ]
                        }
                    }
                }
            }
        }
           stage('Deploy locally'){
                agent any
                steps {
                    script {
                    build job:'Deploy'
                }
            }
        }
    }
}
